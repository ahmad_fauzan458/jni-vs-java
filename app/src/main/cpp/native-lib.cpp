#include <jni.h>
#include <string>

class time_point;

extern "C" JNIEXPORT jstring JNICALL
Java_id_ac_ui_cs_mobileprogramming_ahmad_1fauzan_1amirul_1isnain_coba_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}

extern "C" JNIEXPORT jint JNICALL
Java_id_ac_ui_cs_mobileprogramming_ahmad_1fauzan_1amirul_1isnain_coba_MainActivity_intFromJNI(
        JNIEnv* env,
        jobject obj)
        {
    int i = 0;
    while (i < 1000000) {
        i = i + 5;
    }
    return i;
}
