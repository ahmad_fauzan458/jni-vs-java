package id.ac.ui.cs.mobileprogramming.ahmad_fauzan_amirul_isnain.coba;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button jniButton = findViewById(R.id.jnibutton);
        jniButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("HUWAA","F");
                long startTime = System.nanoTime();
                int number = intFromJNI();
                long endTime = System.nanoTime();

                long duration = (endTime - startTime);

                TextView tv = findViewById(R.id.jni);
                tv.setText(String.valueOf(duration));
            }
        });

        Button javaButton = findViewById(R.id.JavaButton);
        javaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long startTime = System.nanoTime();
                int number = intFromJava();
                long endTime = System.nanoTime();

                long duration = (endTime - startTime);

                TextView tv = findViewById(R.id.java);
                tv.setText(String.valueOf(duration));
            }
        });
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();

    public native int intFromJNI();

    public int intFromJava(){
        int i = 0;
        while (i < 1000000) {
            i = i + 5;
        }
        return i;
    }
}
